# Fontello CLI

This is a fork of the [original Fontello
CLI](https://github.com/paulyoung/fontello-cli), that hasn't been updated in
many years. It is maintained for Postmill's purposes--please don't rely on it.

## Installation
```sh
$ npm install -g fontello-cli
```

## Usage
```
$ fontello-cli --help
```

## Development
```
$ npm run-script build
```
